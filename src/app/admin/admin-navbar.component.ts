import {Component} from '@angular/core';
import {$} from 'protractor';

@Component({
  selector: 'app-admin-navbar-component',
  templateUrl: './admin-navbar.html',
  styleUrls: ['../common/navbar.scss'],
  providers: []
})

export class AdminNavbarComponent {

  logOut() {
    localStorage.setItem('userMail', '');
    localStorage.setItem('jwtToken', '');
  }
}
