import {Component, OnInit} from '@angular/core';
import {TaskService} from '../task/task.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-admin-component',
  templateUrl: './admin.html',
  providers: [TaskService]
})

export class AdminComponent {

  constructor() {
  }

}
