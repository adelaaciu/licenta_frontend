import {Component, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit {
  isVisible = true;

  constructor(private route: Router, private _location: Location) {

  }


  ngOnInit() {
    this.route.events.subscribe(this.onUrlChange.bind(this));
  }

  onUrlChange(changes: SimpleChanges): void {
    if (this.route.url === '/') {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
  }
  backClicked() {
    this._location.back();
  }
}
