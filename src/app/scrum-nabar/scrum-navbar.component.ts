import {Component} from '@angular/core';

@Component({
  selector: 'app-scrum-navbar-component',
  templateUrl: './scrum-navbar.html',
  styleUrls: ['../common/navbar.scss', './scrum-navbar.scss'],
  providers: []
})

export class ScrumNavbarComponent {
  logOut() {
    localStorage.setItem('userMail', '');
    localStorage.setItem('jwtToken', '');
  }
}
