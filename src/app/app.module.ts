import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';


import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {AdminComponent} from './admin/admin.component';
import {AdminNavbarComponent} from './admin/admin-navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProjectComponent} from './project/project.component';
import {ProjectDetailsComponent} from './project/info/project.info.popup.component';
import {ProjectCreateNewComponent} from './project/create.new/project.create.new.component';
import {AddUsersToProjectComponent} from './user/add.users/add.users.to.project.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UserComponent} from './user/user.component';
import {CreateUserComponent} from './user/user-info-popup/user-popup.component';
import {NgDatepickerModule} from 'ng2-datepicker';
import {ProductOwnerComponent} from './product.owner/po.component';
import {ProductOwnerNavbarComponent} from './product.owner/po-navbar.component';
import {ProjectBoardComponent} from './project/project-board/project-board.component';
import {BoardComponent} from './board/board.component';
import {SprintComponent} from './sprint/sprint.component';
import {ScrumNavbarComponent} from './scrum-nabar/scrum-navbar.component';
import {BoardCreateNewComponent} from './board/create.new/board.create.new.component';
import {AddUsersToBoardComponent} from './user/add.users/add.users.to.board';
import {BoardItemsComponent} from './board/board.items/board.items.component';
import {BacklogComponent} from './backlog/backlog.component';
import {TaskInfoComponent} from './task/task.info/task.info.component';
import {SprintTaskListComponent} from './sprint/sprint.task.list/sprint.task.list.component';
import {SprintCardComponent} from './sprint/sprint.cards/sprint.card.component';
import {AddTaskFromBacklogComponent} from './task/add.task.from.backlog/add.task.from.backlog.component';
import {MyAccountComponent} from './my.account/my.account.component';
import {CustomInterceptor} from './common/custom.interceptor';
import {UnauthorizedComponent} from './errors/unauthorized';
import {ReviewComponent} from './review/review.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BurndownChartComponent} from './burndown-chart/burndown.chart.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

const appRoutes: Routes = [
  {path: '#', component: LoginComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'unauthorized', component: UnauthorizedComponent},
  {path: 'admin/projects', component: ProjectComponent},
  {path: 'admin/users', component: UserComponent},
  {path: 'user/account', component: MyAccountComponent},
  {path: 'user', component: ProductOwnerComponent},
  {path: 'user/projects', component: ProjectBoardComponent},
  {path: 'user/projects/:projectName/boards', component: BoardComponent},
  {path: 'user/projects/:projectName/boards/:boardKey/items', component: BoardItemsComponent},
  {path: 'user/projects/:projectName/boards/:boardKey/backlog/:backlogKey', component: BacklogComponent},
  {path: 'user/projects/:projectName/boards/:boardKey/sprints/:sprintKey', component: SprintComponent},
  {path: 'user/projects/:projectName/boards/:boardKey/sprints/:sprintKey/review', component: ReviewComponent},
  {path: 'user/projects/:projectName/boards/:boardKey/sprints/:sprintKey/chart', component: BurndownChartComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    AdminNavbarComponent,
    ProjectComponent,
    ProjectDetailsComponent,
    ProjectCreateNewComponent,
    AddUsersToProjectComponent,
    UserComponent,
    BoardComponent,
    CreateUserComponent,
    ProductOwnerComponent,
    ProductOwnerNavbarComponent,
    ProjectBoardComponent,
    SprintComponent,
    ScrumNavbarComponent,
    BoardCreateNewComponent,
    AddUsersToBoardComponent,
    BoardItemsComponent,
    BacklogComponent,
    TaskInfoComponent,
    SprintTaskListComponent,
    SprintCardComponent,
    AddTaskFromBacklogComponent,
    MyAccountComponent,
    UnauthorizedComponent,
    ReviewComponent,
    BurndownChartComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgDatepickerModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
  ],
  entryComponents: [
    ProjectDetailsComponent,
    ProjectCreateNewComponent,
    AddUsersToProjectComponent,
    CreateUserComponent,
    BoardCreateNewComponent,
    AddUsersToBoardComponent,
    TaskInfoComponent,
    AddTaskFromBacklogComponent
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: CustomInterceptor, multi: true}],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
