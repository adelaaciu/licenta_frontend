import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../user.service';
import {ProjectService} from '../../project/project.service';
import {ProjectUser} from '../../dtos/project.user';
import {BoardService} from '../../board/board.service';
import {Board} from '../../dtos/board';
import {Project} from '../../dtos/project';
import {BoardUser} from '../../dtos/board.users';
import {OutputUser, User} from '../../dtos/user';
import {ProjectUserRole} from '../../dtos/role';

@Component({
  templateUrl: './add.users.to.project.html',
  styleUrls: ['./add.users.to.project.scss'],
  providers: [UserService, ProjectService, BoardService]
})


export class AddUsersToBoardComponent {

  @Input() private projectUsers: OutputUser[];
  @Input() allUsers: ProjectUser[];
  @Input() users: User[];
  @Input() project: Project;
  @Input() board: Board;
  @Input() warningMessage = false;
  outputBoardUsers: BoardUser[];
  searchedValue: string;


  constructor(public activeModal: NgbActiveModal, private userService: UserService, private boardService: BoardService) {
  }

  getAllUsers() {
    this.users = [];
    this.allUsers = [];
    this.userService.findProjectUsersByProjectName(this.project.name).subscribe(projectUserList => {
      this.allUsers = projectUserList;
      this.users = [];

      this.allUsers.forEach(projectUser => {
        console.log('*ROLE*');
        console.log(projectUser.projectRole);
        if (!projectUser.projectRole.includes(ProjectUserRole.PRODUCT_OWNER)) {
          this.createUserList(projectUser);
        }
      });
    });
  }

  private createUserList(projectUser) {
      const user = projectUser.user;
      const userAdded = new User(
        user.role,
        user.mail,
        user.firstName,
        user.lastName,
        user.phoneNumber,
        user.birthday,
      );

      if (this.projectUsers.find(usr =>
        usr.mail === user.mail)) {
        userAdded.isAdded = true;
      } else {
        userAdded.isAdded = false;
      }
      this.users.push(userAdded);

  }

  testIfUserNameIsEmpty() {
    if (this.searchedValue.length === 0) {
      this.getAllUsers();
    }
  }

  searchUser() {
    this.users = [];

    this.userService.findProjectUserListByUserMailContaining(this.searchedValue).subscribe(data => {
      this.allUsers = data;
      this.allUsers.forEach(projectUser => {
        if (!projectUser.projectRole.includes(ProjectUserRole.PRODUCT_OWNER)) {
          this.createUserList(projectUser);
        }
      });
    });
  }


  addUser(userIndex: number) {
    this.users[userIndex].isAdded = true;
  }

  removeUser(userIndex: number) {
    this.users[userIndex].isAdded = false;
  }

  updateUserList() {
    console.log('Before');
    console.log(this.users);
    this.boardService.updateBoardUserList(this.board, this.users).subscribe(data => {
        console.log('Rezultat');
        console.log(data);
        this.activeModal.close(data);
      }
    );

    // this.outputBoardUsers = [];
    // this.users.forEach((x, index) => {
    //   if (x.isAdded) {
    //     const u = new BoardUser(this.allUsers[index], this.board);
    //     this.outputBoardUsers.push(u);
    //   }
    // });

  }

  closeDialog() {
    this.activeModal.close('Close');
  }

  isCurrentUser(user: User): boolean {
    const currentMail = localStorage.getItem('userMail');
    if (user.mail === currentMail) {
      return true;
    }

    return false;
  }

  isPmSelected() {
    return false;
  }
}
