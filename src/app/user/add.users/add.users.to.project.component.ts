import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../user.service';
import {ProjectService} from '../../project/project.service';
import {ProjectUser} from '../../dtos/project.user';
import {Project} from '../../dtos/project';
import {OutputUser, User} from '../../dtos/user';
import {ProjectUserRole, UserRole} from "../../dtos/role";

@Component({
  templateUrl: './add.users.to.project.html',
  styleUrls: ['./add.users.to.project.scss'],
  providers: [UserService, ProjectService]
})


export class AddUsersToProjectComponent {

  @Input() private projectUsers: OutputUser[];
  @Input() private allUsers: OutputUser[];
  @Input() users: User[];
  @Input() project: Project;
  @Input() title: string;
  outputProjectUserList: ProjectUser[];
  searchedValue: string;
  @Input() addProductOwner = false;
  @Input() alreadyAdded: boolean;
  @Input() warningMessageAdmin: boolean;
  @Input() initialPMMail: string;
  private pmIndex: number = -1;

  constructor(public activeModal: NgbActiveModal, private userService: UserService, private projectService: ProjectService) {
  }

  getAllUsers() {
    this.users = [];

    this.userService.getAllUsers().subscribe(data => {
      console.log(data);
      this.allUsers = data;

      this.allUsers.forEach(u => {
        if (u.role !== UserRole.ADMIN && !u.mail.match('anonymous@thinkagile.com')) {
          const userAdded = new User(
            u.role,
            u.mail,
            u.firstName,
            u.lastName,
            u.phoneNumber,
            u.birthday,
          );

          if (this.projectUsers.find(usr =>
              usr.mail === u.mail)) {
            userAdded.isAdded = true;
          } else {
            userAdded.isAdded = false;
          }
          this.users.push(userAdded);
        }
      });

    });
  }

  testIfUserNameIsEmpty() {
    if (this.searchedValue.length === 0) {
      this.getAllUsers();
    }
  }

  searchUser() {
    this.users = [];

    this.userService.getUsersByName(this.searchedValue).subscribe(data => {
      console.log(data);
      this.allUsers = data;

      this.allUsers.forEach(u => {
        if (u.role !== UserRole.ADMIN && !u.mail.match('anonymous@thinkagile.com')) {
          const userAdded = new User(
            u.role,
            u.mail,
            u.firstName,
            u.lastName,
            u.phoneNumber,
            u.birthday,
          );

          if (this.projectUsers.find(usr =>
            usr.mail === u.mail)) {
            userAdded.isAdded = true;
          } else {
            userAdded.isAdded = false;
          }
          this.users.push(userAdded);
        }
      });
    });
  }


  addUser(userIndex: number) {
    if (this.addProductOwner && !this.alreadyAdded) {
      this.alreadyAdded = true;
      console.log(this.pmIndex);
      this.pmIndex = userIndex;
      console.log(this.pmIndex);
    }
    this.users[userIndex].isAdded = true;
  }

  removeUser(userIndex: number) {
    if (this.addProductOwner && this.alreadyAdded) {
      this.alreadyAdded = false;
    }
    this.users[userIndex].isAdded = false;
  }

  updateUserList() {
    this.outputProjectUserList = [];
    if (this.addProductOwner) {
      console.log(this.pmIndex);
      if (this.pmIndex === -1) {
        this.userService.deleteProjectUser(this.project.name, this.initialPMMail).subscribe(data => {
          this.userService.findProjectUsersByProjectName(this.project.name).subscribe(projectUserList => {
            this.outputProjectUserList = projectUserList;
          });

          this.activeModal.close(this.outputProjectUserList);
        });
      }
      else {
        this.userService.addProductOwner(this.project.name, this.users[this.pmIndex].mail).subscribe(projectUser => {
          this.userService.findProjectUsersByProjectName(this.project.name).subscribe(projectUserList => {
            this.outputProjectUserList = projectUserList;
          });

          this.activeModal.close(this.outputProjectUserList);
        });
      }
    } else {
      this.projectService.updateUserList(this.project.name, this.users).subscribe(data => {
        console.log(data);
        this.userService.findProjectUsersByProjectName(this.project.name).subscribe(projectUserList => {
          this.outputProjectUserList = projectUserList;
          this.activeModal.close(this.outputProjectUserList);
        });

      });
    }
  }

  closeDialog() {
    this.activeModal.close('Close');
  }

  isCurrentUser(user: User): boolean {
    return false;
  }

  isPmSelected() {
    if (this.addProductOwner) {
      if (this.alreadyAdded) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  }

}
