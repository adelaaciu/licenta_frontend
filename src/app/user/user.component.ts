import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateUserComponent} from './user-info-popup/user-popup.component';
import {UserService} from './user.service';
import {OutputUser} from '../dtos/user';

@Component({
  selector: 'app-admin-users',
  templateUrl: './user.html',
  styleUrls: ['./user.scss'],
  providers: [UserService]
})
export class UserComponent implements OnInit {
  role: string;
  mail: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  birthday: string;
  searchedValue: string;
  private userList: OutputUser[];

  constructor(private modalService: NgbModal, private userService: UserService) {

  }

  ngOnInit(): void {
    this.getAllUsers();
  }

  displayCreateNewUserPopup() {
    const modalRef = this.modalService.open(CreateUserComponent);
    modalRef.componentInstance.isUpdateActive = false;
    modalRef.componentInstance.title = 'Create new user';
    modalRef.componentInstance.role = 'admin';
    modalRef.result.then(result => {
      if (result !== 'Close') {
        console.log(result);
        this.userList.push(result);
      }
    });
  }


  searchUser() {
    this.userService.getUsersByName(this.searchedValue).subscribe(data => {
      console.log(data);
      this.userList = data;
    });
  }

  removeUser(index: number) {
    console.log(index);
    console.log(this.userList[index].mail);
    this.userService.deleteUser(this.userList[index].mail);
    this.userList.splice(index, 1);
  }

  displayUserInfo(index: number) {
    const modalRef = this.modalService.open(CreateUserComponent);
    modalRef.componentInstance.isUpdateActive = true;
    modalRef.componentInstance.firstName = this.userList[index].firstName;
    modalRef.componentInstance.lastName = this.userList[index].lastName;
    modalRef.componentInstance.mail = this.userList[index].mail;
    modalRef.componentInstance.role = this.userList[index].role;
    modalRef.componentInstance.phoneNumber = this.userList[index].phoneNumber;
    modalRef.componentInstance.birthday = this.userList[index].birthday;
    modalRef.componentInstance.title = 'View/Edit user info';

    modalRef.result.then(result => {
      if (result !== 'Close') {
        console.log(result);
        this.userList[index] = result;
      }
    });
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(data => {
      console.log(data);
      this.userList = data;
    });
  }

  testIfEmpty() {
    if (this.searchedValue.length === 0) {
      this.getAllUsers();
    }
  }

}
