import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ProjectUser} from '../dtos/project.user';
import {OutputUser, User} from '../dtos/user';
import {BoardUser} from "../dtos/board.users";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;


@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  createNewUser(role: string, mail: string, firstName: string, lastName: string, phoneNumber: string, birthday: string) {
    const user = new OutputUser(role, mail, firstName, lastName, phoneNumber, birthday);
    return this.http.post<OutputUser>(baseUrl + '/user', JSON.stringify(user),
    );
  }

  updateUser(role: string, mail: string, firstName: string, lastName: string, phoneNumber: string, birthday: string) {
    const user = new OutputUser(role, mail, firstName, lastName, phoneNumber, birthday);
    return this.http.put<OutputUser>(baseUrl + '/user/update', JSON.stringify(user)
    );
  }

  deleteUser(email: string) {
    const url = `${baseUrl}/user/list/delete`;
    console.log(url);
    return this.http.delete(url,
      {params: {'userMail': email}}
    ).subscribe();
  }

  getAllUsers() {
    return this.http.get<OutputUser[]>(baseUrl + '/user/list');
  }

  getUsersByName(userName: string) {
    return this.http.get <OutputUser[]>(baseUrl + '/user/list/' + userName);
  }

  findProjectUserListByUserMailContaining(userMail: string) {
    return this.http.get<ProjectUser[]>(baseUrl + '/projectUser/list/', {

      params: {'userMail': userMail}
    });
  }

  findProjectUserByUserMail(projectName: string) {
    return this.http.get<ProjectUser>(baseUrl + `/projectUser/list/user/project/${projectName}`);
  }

  findProjectUsersByProjectName(projectName: string) {
    return this.http.get<ProjectUser[]>(baseUrl + '/projectUser/list/project/' + projectName);
  }

  getAllBoardUsers(boardKey: string) {
    return this.http.get<BoardUser[]>(baseUrl + '/boardUser/list/' + boardKey);
  }

  //
  // getBoardUser(mail, boardKey) {
  //   return this.http.get<BoardUser>(baseUrl + `/boardUser/list/${mail}/board/${boardKey}`, {headers: headers});
  // }

  addProductOwner(projectName, pmMail) {
    return this.http.put<ProjectUser>(baseUrl + `/projectUser/list/project/${projectName}/users/owner`, pmMail);
  }

  deleteProjectUser(projectName, userMail) {
    return this.http.delete(baseUrl + `/projectUser/list/project/${projectName}/users/delete`, {
      params: {'userMail': userMail}
    });
  }

  isCurrentUserInRole(role) {
    return this.http.get<boolean>(baseUrl + `/user/userRole/${role}`);

  }

  getCurrentUser() {
    return this.http.get<User>(baseUrl + '/user/loggedUser');
  }

  getBoardUser(boardKey) {
    return this.http.get<BoardUser>(baseUrl + `/boardUser/list/board/${boardKey}`);
  }
}


