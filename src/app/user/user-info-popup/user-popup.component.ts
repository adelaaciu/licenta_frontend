import {Component, Input} from '@angular/core';
import {NgbActiveModal, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../user.service';


@Component({
  selector: 'app-create-user-component',
  templateUrl: './user-popup.html',
  styleUrls: ['./user-popup.scss'],
  providers: [UserService]
})

export class CreateUserComponent {
  @Input() firstName;
  @Input() lastName;
  @Input() mail;
  @Input() role;
  @Input() phoneNumber;
  birthday;
  @Input() isUpdateActive;
  @Input() title;
  canCreateUser = true;
  canUpdate = true;
  @Input() dateModel: NgbDateStruct;
  minDate = {year: 1918, month: 1, day: 1};
  maxDate = {year: 2018, month: 1, day: 1};

  constructor(public activeModal: NgbActiveModal, public userService: UserService) {
  }

  createNewUser() {
    console.log(this.dateModel);
    console.log(this.dateModel.day);
    console.log(this.dateModel.month);

    this.birthday = this.dateModel.year + '-';
    this.dateModel.month < 9 ? this.birthday += '0' + this.dateModel.month : this.birthday += this.dateModel.month;
    this.birthday += '-';
    this.dateModel.day < 9 ? this.birthday += '0' + this.dateModel.day : this.birthday += this.dateModel.day;


    console.log(this.birthday);

    this.userService.createNewUser(
      this.role,
      this.mail,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.birthday).subscribe(data => {
        this.canCreateUser = true;
        console.log(data);
        this.activeModal.close(data);
      },
      err => {
        console.log('in err');
        this.canCreateUser = false;
      });
  }


  updateUser() {
    console.log(this.dateModel);
    console.log(this.dateModel.day);
    console.log(this.dateModel.month);

    this.birthday = this.dateModel.year + '-' + this.dateModel.month + '-' + this.dateModel.day;
    this.userService.updateUser(
      this.role,
      this.mail,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.birthday).subscribe(data => {
        console.log(data);
        this.activeModal.close(data);
        this.canUpdate = true;
      },
      err => {
        this.canUpdate = false;
      });


  }

  getRole() {
    console.log(this.role);
  }

}


