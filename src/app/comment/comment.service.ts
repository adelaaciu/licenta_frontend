import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommentDto} from '../dtos/comments';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;


@Injectable()
export class CommentService {

  constructor(private http: HttpClient) {
  }

  getTaskComments(taskKey) {
    return this.http.get<CommentDto[]>(baseUrl + `/comment/list/${taskKey}`);
  }

  addTaskComments(commentDto: CommentDto) {
    return this.http.post<CommentDto>(baseUrl + '/comment', commentDto);
  }


}
