import {Component, Input, OnInit} from '@angular/core';
import {BoardService} from './board.service';
import {Board} from '../dtos/board';
import {BoardCreateNewComponent} from './create.new/board.create.new.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Project} from '../dtos/project';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from '../project/project.service';
import {UserService} from '../user/user.service';
import {ProjectUserRole} from '../dtos/role';
import {ProjectUser} from '../dtos/project.user';


@Component({
  selector: 'app-board-component',
  templateUrl: './board.html',
  styleUrls: ['./board.scss', '../common/common.scss'],
  providers: [BoardService, ProjectService, UserService]
})

export class BoardComponent implements OnInit {
  @Input()
  boards: Board[] = [];
  projectName;
  project: Project;
  isProductOwner;

  constructor(private userService: UserService, private modalService: NgbModal, private projectService: ProjectService, private  boardService: BoardService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.projectName = params.projectName;
      console.log(this.projectName);

      this.userService.findProjectUserByUserMail(this.projectName).subscribe(userData => {
        const projectUser: ProjectUser =  userData;
        if (projectUser.projectRole === ProjectUserRole.PRODUCT_OWNER) {
          this.isProductOwner = true;
        } else {
          console.log(ProjectUserRole.USER);
          this.isProductOwner = false;
        }
      });

      this.projectService.getProjectByName(this.projectName).subscribe(proj => {
        this.project = proj;
        this.boardService.getProjectBoards(this.projectName).subscribe(boardList => {
          this.boards = boardList;
        });
      });
    });
  }

  createNewBoardPopup() {
    const modalRef = this.modalService.open(BoardCreateNewComponent);
    modalRef.componentInstance.project = this.project;

    modalRef.result.then((result) => {
        if (result !== 'Close') {
          this.boards.push(result);
        }
      }
    );
  }

  goToItem(index) {
    this.boardService.goToItem(this.boards[index]);
  }

}
