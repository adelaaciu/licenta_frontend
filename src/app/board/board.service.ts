import {Injectable} from '@angular/core';
import {Board} from '../dtos/board';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Project} from '../dtos/project';
import {User} from '../dtos/user';
import {BoardUser} from '../dtos/board.users';
import {Router} from '@angular/router';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class BoardService {
  constructor(private router: Router, private http: HttpClient) {
  }

  createNewBoard(boardName: String, project: Project) {
    const board = new Board(boardName, project);
    return this.http.post<Board>(baseUrl + '/board', JSON.stringify(board));
  }

  updateBoardUserList(board: Board, users: User[]) {
    const usersMail: string[] = [];
    users.forEach(x => {
      if (x.isAdded) {
        usersMail.push(x.mail);
      }
    });

    console.log(usersMail);

    return this.http.put<BoardUser[]>(baseUrl + '/boardUser/list/board/' + board.key,
      JSON.stringify(usersMail));
  }

  getProjectBoards(projectName: string) {
    return this.http.get<Board[]>(baseUrl + '/board/list/project/' + projectName);
  }

  goToItem(b: Board) {
    console.log(b.key);
    this.getBoardByKey(b.key).subscribe(result => {
        this.router.navigate([`user/projects/${b.project.name}/boards/${b.key}/items`]);
      }, err => {
        console.log(err.message);
      }
    );
  }

  getBoardByKey(boardKey) {
    return this.http.get<Board>(baseUrl + '/board/' + boardKey);
  }

  deleteBoard(boardKey: String) {
    return this.http.delete(baseUrl + '/board/list/' + boardKey);
  }
}
