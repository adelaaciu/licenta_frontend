import {Component, Inject, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BoardService} from '../board.service';
import {Board} from '../../dtos/board';
import {Project} from '../../dtos/project';

@Component({
  selector: 'app-create-new-board',
  templateUrl: './board.create.new.html',
  styleUrls: ['./board.create.new.scss'],
  providers: [BoardService]
})

export class BoardCreateNewComponent {
  boardName: String;
  canCreateBoard = true;
  board: Board;
  @Input()
  project: Project;

  constructor(public activeModal: NgbActiveModal, private boardService: BoardService) {

  }

  createNewBoard() {
    this.boardService.createNewBoard(this.boardName, this.project).subscribe(data => {
        this.canCreateBoard = true;
        this.board = data;
        this.activeModal.close(data);
      },
      err => {
        this.canCreateBoard = false;
      });

  }

}
