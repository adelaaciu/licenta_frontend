import {BoardService} from '../board.service';
import {Component, OnInit} from '@angular/core';
import {Backlog} from '../../dtos/backlog';
import {Sprint} from '../../dtos/sprint';
import {Board} from '../../dtos/board';
import {AddUsersToBoardComponent} from '../../user/add.users/add.users.to.board';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OutputUser, User} from '../../dtos/user';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../user/user.service';
import {BoardUser} from '../../dtos/board.users';
import {SprintService} from '../../sprint/sprint.service';
import {ProjectBoardService} from '../../project/project-board/project-board.service';
import {BacklogService} from '../../backlog/backlog.service';
import {ProjectUser} from '../../dtos/project.user';
import {ProjectUserRole} from '../../dtos/role';

@Component({
  selector: 'app-board-items-component',
  templateUrl: './board.items.html',
  styleUrls: ['./board.items.scss', '../../common/common.scss'],
  providers: [BoardService, UserService, SprintService, ProjectBoardService, BacklogService]
})

export class BoardItemsComponent implements OnInit {
  backlog: Backlog;
  sprints: Sprint[] = [];
  board: Board;
  isProductOwner;

  constructor(private modalService: NgbModal,
              private boardService: BoardService,
              private backlogService: BacklogService,
              private userService: UserService,
              private sprintService: SprintService,
              private projectBoardService: ProjectBoardService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log(params);
      console.log(params.boardKey);
      const boardKey = params.boardKey;

      this.userService.findProjectUserByUserMail(params.projectName).subscribe(userData => {
        const projectUser: ProjectUser = userData;
        if (projectUser.projectRole === ProjectUserRole.PRODUCT_OWNER) {
          this.isProductOwner = true;
        } else {
          console.log(ProjectUserRole.USER);
          this.isProductOwner = false;
        }
      });


      this.boardService.getBoardByKey(boardKey).subscribe(boardDto => {
        console.log(boardDto);
        this.board = boardDto;
        this.sprints = this.board.sprints;
        this.backlog = this.board.backlog;
      });
    });
  }

  goToBacklog() {
    this.backlogService.goToBacklog(this.board);
  }

  goToSprint(index) {
    this.sprintService.goToSprint(this.board, this.sprints[index].key);
  }

  goBackToBoard() {
    const project = this.board.project;
    this.boardService.getProjectBoards(project.name).subscribe(boardList => {
      project.boards = boardList;
      this.projectBoardService.goToBoard(project);
    });
  }

  deleteBoard() {
    this.boardService.deleteBoard(this.board.key).subscribe(data => {
      this.goBackToBoard();
    });
  }

  createNewSprint() {
    const sprint = new Sprint(this.board);
    console.log(this.board);
    console.log(sprint);
    this.sprintService.createSprint(sprint).subscribe(s => {
      this.board.sprints.push(s);
    });
  }

  addUsersToBoard() {
    const project = this.board.project;
    console.log(project);
    this.userService.findProjectUsersByProjectName(project.name).subscribe(data => {
        const projectUsers = data;

        const modalRef = this.modalService.open(AddUsersToBoardComponent);
        const outputUsers: OutputUser[] = [];
        const users: User[] = [];
        let boardUsers: BoardUser[] = [];
        this.userService.getAllBoardUsers(this.board.key).subscribe(boardUserList => {

          console.log('Lista board users');
          console.log(boardUserList);
          console.log(JSON.stringify(boardUserList));

          boardUsers = boardUserList;

          if (projectUsers != null) {
            projectUsers.forEach(value => {
              outputUsers.push(value.user);
              const u = value.user;

              if (!value.projectRole.includes(ProjectUserRole.PRODUCT_OWNER)) {
                const userAdded = new User(
                  u.role,
                  u.mail,
                  u.firstName,
                  u.lastName,
                  u.phoneNumber,
                  u.birthday);
                userAdded.isAdded = false;
                users.push(userAdded);
              }
            });

            console.log('Utilizatori users');

            console.log(users);

            users.forEach(user => {
              if (boardUsers != null) {
                boardUsers.forEach(boardUser => {
                  let exist;
                  exist = false;
                  if (boardUser.user.user.mail === user.mail) {
                    exist = true;
                  }

                  if (exist === true) {
                    user.isAdded = true;
                  }
                });
              }
            });

            modalRef.componentInstance.allUsers = projectUsers;
            modalRef.componentInstance.projectUsers = outputUsers;
            modalRef.componentInstance.users = users;

            if (users === null || users.length === 0) {
              modalRef.componentInstance.warningMessage = true;
            }
            modalRef.componentInstance.project = project;
            modalRef.componentInstance.board = this.board;

            console.log(project.users);
            console.log(this.board);

            modalRef.result.then(result => {
              if (result !== 'Close') {
                console.log('result ');
                console.log(result);
                this.board.boardUser = result;
              }
            });
          }
        });
      }
    );
  }

  isSprintDone(index) {
    if (null != this.sprints[index].endDate && this.sprints[index].closed) {
      return true;
    }
    return false;
  }

  isSprintActive(index) {
    if (!this.sprints[index].closed) {
      return true;
    }
    return false;
  }
}
