export class BurndownChartData {
  name: string;
  series: NameValue[];

  constructor() {
  }

}

export class NameValue {
  name: any;
  value: any;

  constructor() {
  }
}
