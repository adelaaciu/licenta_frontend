import {Component, Input} from '@angular/core';
import {TaskService} from '../task/task.service';
import {BurndownChartData} from './burndown.chart.data';

@Component({
  selector: 'app-burndown-chart-component',
  templateUrl: './burndown.chart.html',
  providers: [TaskService]
})


export class BurndownChartComponent {
  sprintDuration = 3;


  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Day';
  showYAxisLabel = true;
  yAxisLabel = 'Story points';

  @Input('multi')
  multi: BurndownChartData[];
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

}
