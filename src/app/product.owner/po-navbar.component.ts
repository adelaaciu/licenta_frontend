import {Component} from '@angular/core';
import {$} from 'protractor';

@Component({
  selector: 'app-product-owner-navbar-component',
  templateUrl: './product-owner-navbar.html',
  styleUrls: ['../common/navbar.scss'],
  providers: []
})

export class ProductOwnerNavbarComponent {
  isBoardVisible = false;
  isBordDetailsVisile = false;

  logOut() {
    // localStorage.setItem('userMail', '');
    // localStorage.setItem('jwtToken', '');
    localStorage.clear();
  }
}
