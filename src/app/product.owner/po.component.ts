import {Component, OnInit} from '@angular/core';
import {TaskService} from '../task/task.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskDto} from '../dtos/task';
import {Dashboad} from '../dtos/dashboad';

@Component({
  selector: 'app-product-owner-component',
  templateUrl: './po.html',
  styleUrls: ['po.scss', '../common/common.scss'],
  providers: [TaskService]
})

export class ProductOwnerComponent implements OnInit {
  tasks: TaskDto[] = [];
  dashboard: Dashboad[] = [];

  constructor(private route: ActivatedRoute, private taskService: TaskService, private router: Router) {
  }

  ngOnInit(): void {
    this.taskService.getLatestUpdates().subscribe(dashboardData => {
      console.log(dashboardData);
      this.dashboard = dashboardData;
      this.dashboard.forEach(item => {
        this.tasks.push(item.taskDto);
      });
    });
  }

  goToTask(index) {
    if (this.dashboard[index].sprintDto != null) {
      this.goToSprintTask(this.dashboard[index]);
    } else {
      this.goToBacklogTask(this.dashboard[index]);
    }
  }

  goToBacklogTask(dashboard: Dashboad) {
    this.router.navigate([`user/projects/${dashboard.projectDto.name}/boards/${dashboard.boardDto.key}/backlog/${dashboard.backlogDto.key}`]);
  }

  goToSprintTask(dashboard: Dashboad) {
    console.log('backlog');
    this.router.navigate([`user/projects/${dashboard.projectDto.name}/boards/${dashboard.boardDto.key}/sprints/${dashboard.sprintDto.key}`]);
  }

}
