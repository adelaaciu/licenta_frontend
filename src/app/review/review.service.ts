import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Review} from "../dtos/review";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class ReviewService {
  constructor(private http: HttpClient) {

  }

  addReview(review: Review) {
    return this.http.post(baseUrl + '/review', review);
  }

  getSprintReviewsByCategory(sprintKey, category) {
    return this.http.get<Review[]>(baseUrl + `/review/sprint/${sprintKey}/category/${category}`);
  }

}
