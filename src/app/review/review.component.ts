import {Component, OnInit} from '@angular/core';
import {Review} from '../dtos/review';
import {ReviewService} from './review.service';
import {SprintService} from '../sprint/sprint.service';
import {ActivatedRoute} from '@angular/router';
import {Sprint} from '../dtos/sprint';
import {ReviewCategory} from '../dtos/role';
import {UserService} from '../user/user.service';
import {BoardUser} from '../dtos/board.users';

@Component({
  selector: 'app-review-component',
  templateUrl: 'review.html',
  styleUrls: ['review.scss', '../common/common.scss'],
  providers: [ReviewService, SprintService, UserService]
})

export class ReviewComponent implements OnInit {
  sprint: Sprint;

  reviewList: Review[] = [];
  continueDoing: Review[] = [];
  stopDoing: Review[] = [];
  startDoing: Review[] = [];
  stop_txt = '';
  start_txt = '';
  continue_txt = '';
  private user: BoardUser;

  constructor(private reviewService: ReviewService, private sprintService: SprintService, private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const sprintKey = params.sprintKey;
      this.sprintService.getCurrentSprint(sprintKey).subscribe(sprintData => {
        this.sprint = sprintData;
      });

      this.userService.getBoardUser(params.boardKey).subscribe(currentUser => {
        this.user = currentUser;

        this.reviewService.getSprintReviewsByCategory(sprintKey, ReviewCategory.CONTINUE_DOING).subscribe(reviews => {
          this.continueDoing = reviews;
        });


        this.reviewService.getSprintReviewsByCategory(sprintKey, ReviewCategory.START_DOING).subscribe(reviews => {
          this.startDoing = reviews;
        });

        this.reviewService.getSprintReviewsByCategory(sprintKey, ReviewCategory.STOP_DOING).subscribe(reviews => {
          this.stopDoing = reviews;
        });
      });
    });
  }

  addToContinue() {
    if (this.continue_txt !== '') {
      const review: Review = new Review();
      review.message = this.continue_txt;
      review.sprint = this.sprint;
      review.category = ReviewCategory.CONTINUE_DOING;
      review.user = this.user;
      this.continueDoing.push(review);
      this.continue_txt = '';

      this.reviewService.addReview(review).subscribe(reviewData => {
        console.log(reviewData);
      });
    }

  }

  addToStopDoing() {
    if (this.stop_txt !== '') {
      const review: Review = new Review();
      review.message = this.stop_txt;
      review.sprint = this.sprint;
      review.category = ReviewCategory.STOP_DOING;
      review.user = this.user;
      this.stopDoing.push(review);
      this.stop_txt = '';

      this.reviewService.addReview(review).subscribe(reviewData => {
        console.log(reviewData);
      });
    }
  }

  addToStartDoing() {
    if (this.start_txt !== '') {
      const review: Review = new Review();
      review.message = this.start_txt;
      review.sprint = this.sprint;
      review.category = ReviewCategory.START_DOING;
      review.user = this.user;
      this.startDoing.push(review);
      this.start_txt = '';

      this.reviewService.addReview(review).subscribe(reviewData => {
        console.log(reviewData);
      });
    }
  }
}
