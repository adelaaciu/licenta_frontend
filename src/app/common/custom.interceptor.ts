import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Router} from '@angular/router';

@Injectable()
export class CustomInterceptor implements HttpInterceptor {
  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/json'
    });
    const authReq = req.clone({
      headers: headers
    });
    return next.handle(authReq).catch(x => this.handleAuthError(x));
  }


  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    console.log(this.router);
    console.log(this.router.url);
    if (this.router.url !== '/') {

      if (err.status === 401 || err.status === 403) {
        this.router.navigate(['unauthorized']);
        return Observable.of(err.message);
      }
    }
    return Observable.throw(err);
  }

}
