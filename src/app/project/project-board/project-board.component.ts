import {Component} from '@angular/core';
import {ProjectBoardService} from './project-board.service';
import {Project} from '../../dtos/project';

@Component({
  selector: 'app-project-board',
  templateUrl: './project-board.html',
  styleUrls: ['./project-board.scss', '../../common/common.scss'],
  providers: [ProjectBoardService]
})

export class ProjectBoardComponent {
  projects: Project[];

  constructor(private projectBoardService: ProjectBoardService) {
    this.getProjectByUser();
  }

  goToBoard(i) {
    console.log('Index ' + i);
    this.projectBoardService.goToBoard(this.projects[i]);
  }

  getProjectByUser() {
    this.projectBoardService.getProjectByUser().subscribe(data => {
      this.projects = data;
      console.log(data);
    }, err => {
      console.log(err.message);
    });
  }

}
