import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Project} from '../../dtos/project';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;


@Injectable()
export class ProjectBoardService {

  constructor(private router: Router, private http: HttpClient) {
  }

  goToBoard(project: Project) {
    this.router.navigate([`user/projects/${project.name}/boards`]);
  }

  getProjectByUser() {
    return this.http.get<Project[]>(baseUrl + '/project/list/users', );
  }
}

