import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {OutputUser, User} from '../dtos/user';
import {Project} from "../dtos/project";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class ProjectService {

  constructor(private http: HttpClient) {
  }


  createNewProject(projectName: string, projectDescription: string) {
    const project = new Project(projectName, projectDescription);
    project.users = [];
    console.log(JSON.stringify(project));
    return this.http.post<Project>(baseUrl + '/project', JSON.stringify(project));
  }

  deleteProject(name: string) {
    const url = `${baseUrl}/project/list/delete`;
    console.log(url);
    return this.http.put(url, name
    ).subscribe();
  }


  getAllProjects() {
    return this.http.get<Project[]>(baseUrl + '/project/list');
  }

  getProjectByNameContaining(projectName: string) {
    return this.http.get <Project[]>(baseUrl + '/project/list/' + projectName);
  }

  getProjectByName(projectName: string) {
    return this.http.get <Project>(baseUrl + '/project/' + projectName);
  }

  updateUserList(projectName, users: User[]) {
    const usersMail: string[] = [];
    users.forEach(x => {
      if (x.isAdded) {
        usersMail.push(x.mail);
      }
    });

    return this.http.put<OutputUser[]>(baseUrl + `/projectUser/list/project/${projectName}/users`,
      JSON.stringify(usersMail));
  }
}

