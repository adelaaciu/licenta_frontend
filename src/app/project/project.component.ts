import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProjectDetailsComponent} from './info/project.info.popup.component';
import {ProjectService} from './project.service';
import {ProjectCreateNewComponent} from './create.new/project.create.new.component';
import {AddUsersToProjectComponent} from '../user/add.users/add.users.to.project.component';
import {UserService} from '../user/user.service';
import {ProjectUser} from '../dtos/project.user';
import {Project} from '../dtos/project';
import {OutputUser, User} from '../dtos/user';
import {ProjectUserRole, UserRole} from '../dtos/role';

@Component({
  selector: 'app-admin-projects',
  templateUrl: './project.html',
  styleUrls: ['./project.scss'],
  providers: [ProjectService, UserService]
})
export class ProjectComponent implements OnInit {

  private projectName: string;
  private projectDescription: string;
  private projects: Project[];
  private userList: ProjectUser[];
  private allUsersFomDB: OutputUser[];

  constructor(private modalService: NgbModal, private projectService: ProjectService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.getAllProjects();
    this.userService.getAllUsers().subscribe(data => {
      this.allUsersFomDB = data;
    });
  }

  searchProject() {
    this.projectService.getProjectByNameContaining(this.projectName).subscribe(data => {
      this.projects = data;
    });
  }

  displayCreateNewProjectPopup() {
    this.modalService.open(ProjectCreateNewComponent).result.then((result) => {
        if (result !== 'Close') {
          this.projects.push(result);
        }
      }
    );
  }

  displayProjectInfo(projectIndex: number) {
    const modalRef = this.modalService.open(ProjectDetailsComponent);
    modalRef.componentInstance.projectName = this.projects[projectIndex].name;
    modalRef.componentInstance.projectDescription = this.projects[projectIndex].description;
    this.userList = this.projects[projectIndex].users;
    console.log(this.userList);
    if (this.userList === null || this.userList.length === 0) {
      modalRef.componentInstance.projectEmployeeList = 'Users not added yet.';
    } else {
      let usrStringList = '';
      this.userList.forEach(value => {
        const usr: OutputUser = value.user;
        usrStringList += usr.firstName
          + usr.lastName + ' ';
      });

      modalRef.componentInstance.projectEmployeeList = usrStringList;
    }
  }

  displayUsersPopUp(projectIndex: number) {
    const modalRef = this.modalService.open(AddUsersToProjectComponent);

    modalRef.componentInstance.projectUsers = this.projects[projectIndex].users;
    modalRef.componentInstance.users = this.getAllUsers(projectIndex);
    modalRef.componentInstance.project = this.projects[projectIndex];
    modalRef.componentInstance.title = 'Update users list';
    modalRef.componentInstance.addProductOwner = false;
    modalRef.componentInstance.alreadyAdded = false;

    if (this.allUsersFomDB === null || this.allUsersFomDB.length === 0) {
      modalRef.componentInstance.warningMessageAdmin = true;
    }

    modalRef.result.then(result => {
      if (result !== 'Close') {
        this.userService.findProjectUsersByProjectName(this.projects[projectIndex].name).subscribe(projectUserList => {
          this.projects[projectIndex].users = projectUserList;
        });
      }
    });


  }

  displayPMUsersPopUp(projectIndex: number) {
    const modalRef = this.modalService.open(AddUsersToProjectComponent);
    const users: OutputUser[] = [];
    if (this.projects[projectIndex].users !== null) {
      this.projects[projectIndex].users.forEach(value => {
        if (value.projectRole === ProjectUserRole.PRODUCT_OWNER) {
          modalRef.componentInstance.alreadyAdded = true;
          modalRef.componentInstance.initialPMMail = value.user.mail;
        }
      });
    } else {
      modalRef.componentInstance.alreadyAdded = false;
      modalRef.componentInstance.initialPMMail = '';
    }

    modalRef.componentInstance.projectUsers = this.projects[projectIndex].users;
    modalRef.componentInstance.users = this.getAllPmUsers(projectIndex);


    if (this.allUsersFomDB === null || this.allUsersFomDB.length === 0) {
      modalRef.componentInstance.warningMessageAdmin = true;
    }
    modalRef.componentInstance.project = this.projects[projectIndex];
    modalRef.componentInstance.title = 'Update PM list';
    modalRef.componentInstance.addProductOwner = true;

    modalRef.result.then(result => {
      if (result !== 'Close') {
        this.userService.findProjectUsersByProjectName(this.projects[projectIndex].name).subscribe(projectUserList => {
          this.projects[projectIndex].users = projectUserList;
        });
      }
    });
  }

  getAllUsers(index: number) {
    const users: User[] = [];
    this.userService.getAllUsers().subscribe(data => {
      const allUsers: OutputUser[] = data;

      allUsers.forEach(u => {
        if (u.role !== UserRole.ADMIN && !u.role.match('anonymous')) {

          const userAdded = new User(
            u.role,
            u.mail,
            u.firstName,
            u.lastName,
            u.phoneNumber,
            u.birthday,
          );

          if (([] !== (this.projects[index].users) && null !== (this.projects[index].users))
            && (this.projects[index].users).find(usr =>
              usr.user.mail === u.mail && usr.projectRole === ProjectUserRole.USER)) {
            userAdded.isAdded = true;
          } else {
            userAdded.isAdded = false;
          }
          users.push(userAdded);
        }
      });


    });

    return users;
  }

  getAllPmUsers(index: number): OutputUser[] {
    const users: User[] = [];
    this.userService.getAllUsers().subscribe(data => {
      const allUsers: OutputUser[] = data;

      allUsers.forEach(u => {
        if (u.role !== UserRole.ADMIN) {
          const userAdded = new User(
            u.role,
            u.mail,
            u.firstName,
            u.lastName,
            u.phoneNumber,
            u.birthday,
          );

          if (([] !== (this.projects[index].users) && null !== (this.projects[index].users))
            && (this.projects[index].users).find(usr =>
              usr.user.mail === u.mail && usr.projectRole === ProjectUserRole.PRODUCT_OWNER)) {
            userAdded.isAdded = true;
          } else {
            userAdded.isAdded = false;
          }
          users.push(userAdded);
        }
      });
    });
    return users;
  }

  removeProject(projectIndex: number) {
    this.projectService.deleteProject(this.projects[projectIndex].name);
    this.projects.splice(projectIndex, 1);
  }

  testIfEmpty() {
    if (this.projectName.length === 0) {
      this.getAllProjects();
    }
  }

  private getAllProjects() {
    this.projectService.getAllProjects().subscribe(data => {
      this.projects = data;
    });
  }
}

