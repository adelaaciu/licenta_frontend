import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Input} from '@angular/core';
import {ProjectService} from "../project.service";
import {Project} from "../../dtos/project";

@Component({
  selector: 'app-create-new-project',
  templateUrl: './project.create.new.html',
  styleUrls: ['./project.create.new.scss'],
  providers: [ProjectService]
})

export class ProjectCreateNewComponent {
  private projectName: string;
  private projectDescription: string;
  private canCreateProject = true;
  private project: Project;
  constructor(public activeModal: NgbActiveModal, private projectService: ProjectService) {
  }

  createNewProject() {
    this.projectService.createNewProject(this.projectName, this.projectDescription).subscribe(data => {
        this.canCreateProject = true;
        this.project = data;
        this.activeModal.close(data);
      },
      err => {
        this.canCreateProject = false;
      });
  }

}
