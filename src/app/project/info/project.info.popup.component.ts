import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-project-details',
  templateUrl: './project.info.popup.html',
  styleUrls: ['./project.info.popup.scss']
})

export class ProjectDetailsComponent {
  @Input() projectName;
  @Input() projectDescription;
  @Input() projectEmployeeList: string[];
  constructor(public activeModal: NgbActiveModal) {}
}
