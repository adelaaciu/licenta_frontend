import {Project} from './project';
import {Backlog} from '../dtos/backlog';
import {Sprint} from '../dtos/sprint';
import {BoardUser} from '../dtos/board.users';

export class Board {
  key;
  name;
  backlog: Backlog;
  project: Project;
  sprints: Sprint[];
  boardUser: BoardUser[];

  constructor(name, project) {
    this.name = name;
    this.project = project;
    this.backlog = null;
    this.sprints = [];
    this.boardUser = [];
  }
}
