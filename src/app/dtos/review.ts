import {BoardUser} from './board.users';
import {Sprint} from './sprint';

export class Review {
  category: string;
  user: BoardUser;
  message: string;
  sprint: Sprint;

  constructor() {

  }
}

