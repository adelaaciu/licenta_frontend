import {ProjectUser} from './project.user';
import {Board} from './board';

export class Project {
  name: string;
  description: string;
  users: ProjectUser[];
  boards: Board[];

  constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
    this.users = null;
    this.boards = null;
  }
}
