import {BoardUser} from './board.users';
import {CommentDto} from './comments';

export  class  TaskDto {
  key;
  title;
  description;
  status;
  watcher: BoardUser;
  assignee: BoardUser;
  startDate;
  endDate;
  actualizationDate;
  comments: CommentDto[];
  storyPoints: number;
  priority: number;

  constructor(title, description, status, watcher: BoardUser, assignee: BoardUser, comments: CommentDto[], storyPoints: number, priority: number) {
    this.title = title;
    this.description = description;
    this.status = status;
    this.watcher = watcher;
    this.assignee = assignee;
    this.comments = comments;
    this.storyPoints = storyPoints;
    this.priority = priority;
  }
}
