import {TaskDto} from './task';
import {Project} from './project';
import {Board} from './board';
import {Backlog} from './backlog';
import {Sprint} from './sprint';

export class Dashboad {
  taskDto: TaskDto;
  projectDto: Project;
  boardDto: Board;
  backlogDto: Backlog;
  sprintDto: Sprint;
}
