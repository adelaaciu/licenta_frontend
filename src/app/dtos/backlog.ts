import {BacklogTask} from './backlog.task';

export  class  Backlog {
key;
backlogTasks: BacklogTask[];

  constructor(key) {
    this.key = key;
    this.backlogTasks = [];
  }
}
