export enum UserRole {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER'
}

export  enum ProjectUserRole {
  PRODUCT_OWNER = 'product_owner',
  USER = 'user'
}


export enum ReviewCategory {
  CONTINUE_DOING = 'continue_doing',
  START_DOING = 'start_doing',
  STOP_DOING = 'stop_doing'
}
