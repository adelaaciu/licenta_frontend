import {ProjectUser} from './project.user';
import {Board} from './board';

export class BoardUser {
  user: ProjectUser;
  board: Board;

  constructor(user: ProjectUser, board: Board) {
    this.user = user;
    this.board = board;
  }
}
