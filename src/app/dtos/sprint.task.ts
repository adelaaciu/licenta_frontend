import {Sprint} from './sprint';
import {BoardUser} from './board.users';
import {TaskDto} from './task';
import {CommentDto} from "./comments";

export class SprintTask extends TaskDto {
  sprint: Sprint;
  constructor(sprint: Sprint, title, description, status, watcher: BoardUser, assignee: BoardUser, comments: CommentDto[], storyPoints: number, priority: number) {
    super(title, description, status, watcher, assignee, comments, storyPoints, priority);
    this.sprint = sprint;
  }
}
