export class OutputUser {
  role: string;
  mail: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  birthday: string;


  constructor(role: string, mail: string, firstName: string, lastName: string, phoneNumber: string, birthday: string) {
    this.role = role;
    this.mail = mail;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.birthday = birthday;
  }

}

export class User extends OutputUser {
  isAdded: boolean;
}
