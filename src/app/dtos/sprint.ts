import {SprintTask} from './sprint.task';
import {Board} from "./board";

export class Sprint {
  key;
  sprintNo;
  board: Board;
  sprintTasks: SprintTask[];
  startDate: Date;
  endDate;
  days
  closed: boolean;

  constructor(board: Board) {
    this.board = board;
  }
}
