import {Project} from './project';
import {OutputUser} from './user';

export class ProjectUser {
  project: Project;
  user: OutputUser;
  projectRole: string

  constructor(project: Project, user: OutputUser) {
    this.project = project;
    this.user = user;
  }
}
