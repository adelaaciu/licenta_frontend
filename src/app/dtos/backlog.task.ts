import {Backlog} from './backlog';
import {TaskDto} from './task';
import {CommentDto} from "./comments";
import {BoardUser} from "./board.users";

export class BacklogTask extends TaskDto {
  backlog: Backlog;

  constructor(backlog: Backlog, title, description, status, watcher: BoardUser, assignee: BoardUser, comments: CommentDto[], storyPoints: number, priority: number) {
    super(title, description, status, watcher, assignee, comments, storyPoints, priority)
    this.backlog = backlog;
  }
}
