export class CommentDto {
  key;
  description;
  userEmail: String;
  taskKey: String;

  constructor(description, userMail: String, taskKey: String) {
    this.description = description;
    this.userEmail = userMail;
    this.taskKey = taskKey;
  }
}
