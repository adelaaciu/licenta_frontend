import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../../task/task.service';
import {TaskDto} from '../../dtos/task';
import {TaskInfoComponent} from '../../task/task.info/task.info.component';
import {Sprint} from '../../dtos/sprint';
import {CommentService} from '../../comment/comment.service';
import {UserService} from '../../user/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sprint-task-list-component',
  templateUrl: './sprint.task.list.html',
  styleUrls: ['../sprint.scss', '../../common/common.scss'],
  providers: [TaskService]
})

export class SprintTaskListComponent implements OnInit {
  @Input('sprintTasks') sprintTasks: TaskDto[] = [];
  @Input() sprint: Sprint;

  constructor(private route: ActivatedRoute, private taskService: TaskService,
              private modalService: NgbModal, private userService: UserService, private commentService: CommentService) {
  }

  ngOnInit(): void {

  }

  displayTaskInfo(index) {
    this.route.params.subscribe(params => {
      const boardKey = params.boardKey;
      console.log('Task info ');
      console.log(boardKey);
      console.log(this.sprint);

      this.userService.getAllBoardUsers(boardKey).subscribe(users => {

        this.commentService.getTaskComments(this.sprintTasks[index].key).subscribe(comm => {
          const modalRef = this.modalService.open(TaskInfoComponent);
          modalRef.componentInstance.boardKey = boardKey;
          modalRef.componentInstance.boardUsers = users;
          modalRef.componentInstance.comments = comm;

          modalRef.componentInstance.isCreationActivated = false;
          modalRef.componentInstance.task = this.sprintTasks[index];
          modalRef.componentInstance.sprint = this.sprint;

          modalRef.componentInstance.taskTitle = this.sprintTasks[index].title;
          modalRef.componentInstance.taskKey = this.sprintTasks[index].key;
          modalRef.componentInstance.description = this.sprintTasks[index].description;
          modalRef.componentInstance.status = this.sprintTasks[index].status;
          if (null != this.sprintTasks[index].watcher) {
            modalRef.componentInstance.watcherMail = this.sprintTasks[index].watcher.user.user.mail;
          }
          modalRef.componentInstance.watcher = this.sprintTasks[index].watcher;
          modalRef.componentInstance.assignee = this.sprintTasks[index].assignee;
          if (null != this.sprintTasks[index].assignee) {
            modalRef.componentInstance.assigneeMail = this.sprintTasks[index].assignee.user.user.mail;
          }
          modalRef.componentInstance.comments = this.sprintTasks[index].comments;
          modalRef.componentInstance.storyPoints = this.sprintTasks[index].storyPoints;
          modalRef.componentInstance.priority = this.sprintTasks[index].priority;

          modalRef.result.then((result) => {
              if (result !== 'Close') {
                this.sprintTasks[index] = result;
              }
            }
          );
        });
      });
    });
  }

  removeTask(index) {
    this.taskService.removeTaskFromSprint(this.sprint.key, this.sprintTasks[index].key).subscribe(data => {
      this.sprintTasks.splice(index, 1);
    });
  }

}
