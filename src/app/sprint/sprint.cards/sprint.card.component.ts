import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../../task/task.service';
import {TaskInfoComponent} from '../../task/task.info/task.info.component';
import {CommentService} from '../../comment/comment.service';
import {UserService} from '../../user/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Backlog} from '../../dtos/backlog';
import {TaskDto} from '../../dtos/task';
import {SprintService} from '../sprint.service';

@Component({
  selector: 'app-sprint-card-list-component',
  templateUrl: './sprint.cards.html',
  styleUrls: ['../sprint.scss', './sprint.cards.scss', '../../common/common.scss'],
  providers: [TaskService, UserService, CommentService, SprintService]
})

export class SprintCardComponent implements OnInit {
  @Input() sprintTasks: TaskDto[] = [];
  sprintKey: string;
  statusList: string[] = [
    'To-Do',
    'Dev-implementation',
    'Dev-testing',
    'Review',
    'QA',
    'Done'
  ];
  backlog: Backlog;


  constructor(private route: ActivatedRoute,
              private taskService: TaskService,
              private modalService: NgbModal,
              private userService: UserService,
              private commentService: CommentService,
              private sprintService: SprintService) {
  }

  ngOnInit(): void {
  }

  changeToNextStatus(i) {
    const currentStatus = this.sprintTasks[i].status;
    let nextStatus = this.statusList[0];

    this.statusList.forEach((status, index) => {
      if (currentStatus === status) {
        nextStatus = this.statusList[index + 1];
      }
    });

    this.sprintTasks[i].status = nextStatus;
    this.taskService.changeStatus(this.sprintTasks[i].key, nextStatus).subscribe(data => {
      console.log(data);
    });
  }

  changeToPrevStatus(i) {
    const currentStatus = this.sprintTasks[i].status;
    let prevStatus = this.statusList[0];

    this.statusList.forEach((status, index) => {
      if (currentStatus === status) {
        prevStatus = this.statusList[index - 1];
      }
    });

    this.sprintTasks[i].status = prevStatus;
    this.taskService.changeStatus(this.sprintTasks[i].key, prevStatus).subscribe(data => {
      console.log(data);
    });
  }

  displayTaskInfo(index) {
    this.route.params.subscribe(params => {
      console.log(params.boardKey);
      this.userService.getAllBoardUsers(params.boardKey).subscribe(users => {
        this.sprintTasks.forEach(x => {
          console.log(x);
        });
        this.commentService.getTaskComments(this.sprintTasks[index].key).subscribe(comm => {
          const modalRef = this.modalService.open(TaskInfoComponent);
          modalRef.componentInstance.boardUsers = users;
          modalRef.componentInstance.comments = comm;
          modalRef.componentInstance.isCreationActivated = false;
          modalRef.componentInstance.backlog = this.backlog;

          modalRef.result.then((result) => {
              if (result !== 'Close') {
                this.sprintTasks[index] = result;
              }
            }
          );
        });
      });
    });

  }
}
