import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Board} from "../dtos/board";
import {Sprint} from "../dtos/sprint";
import {ActivatedRoute, Router} from "@angular/router";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class SprintService {

  constructor(private router: Router, private http: HttpClient) {
  }

  createSprint(sprint: Sprint) {
    return this.http.post<Sprint>(baseUrl + '/sprint', JSON.stringify(sprint));
  }

  getSprints(boardKey) {
    return this.http.get<Sprint[]>(baseUrl + `/sprint/list/${boardKey}`);
  }

  goToSprint(board: Board, sprintKey) {
    this.router.navigate([`user/projects/${board.project.name}/boards/${board.key}/sprints/${sprintKey}`]);
  }

  startSprint(sprintKey, sprintDays) {
    return this.http.put<Sprint>(baseUrl + `/sprint/list/${sprintKey}/start`, sprintDays);
  }

  endSprint(sprintKey) {
    return this.http.put<Sprint>(baseUrl + `/sprint/list/${sprintKey}/end`, null);
  }

  goToBoardItems(activeRoute: ActivatedRoute) {
    activeRoute.params.subscribe(params => {
      this.router.navigate([`user/projects/${params.projectName}/boards/${params.boardKey}/items`]);
    });
  }

  getCurrentSprint(sprintKey: string) {
    return this.http.get<Sprint>(baseUrl + `/sprint/${sprintKey}`);
  }

  getActiveSprint(boardKey) {
    return this.http.get<Sprint>(baseUrl + `/sprint/list/active/${boardKey}`);
  }

  deleteSprint(sprintKey) {
    return this.http.delete(baseUrl + `/sprint/list/${sprintKey}`);
  }
}
