import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SprintService} from './sprint.service';
import {TaskService} from '../task/task.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user/user.service';
import {CommentService} from '../comment/comment.service';
import {Sprint} from '../dtos/sprint';
import {TaskDto} from '../dtos/task';
import {AddTaskFromBacklogComponent} from '../task/add.task.from.backlog/add.task.from.backlog.component';
import {BacklogService} from '../backlog/backlog.service';
import {BoardService} from '../board/board.service';
import {Board} from '../dtos/board';
import {ProjectUser} from '../dtos/project.user';
import {ProjectUserRole} from '../dtos/role';
import {SprintTask} from '../dtos/sprint.task';
import {BurndownChartData, NameValue} from '../burndown-chart/burndown.chart.data';


@Component({
  selector: 'app-sprint',
  templateUrl: './sprint.html',
  styleUrls: ['./sprint.scss', '../common/common.scss'],
  providers: [SprintService, TaskService, UserService, CommentService, BacklogService, BoardService],
})

export class SprintComponent implements OnInit {
  sprintKey: string;
  canEndSprint = false;
  canStartSprint = false;
  isSprintBoardActive = false;
  isListVisible = true;
  sprint: Sprint;
  activeSprint: Sprint;
  sprintTasks: TaskDto[] = [];
  taskTitle: string;
  canDeleteSprint;
  isProductOwner;
  endedSprint;
  showChart = false;
  isCardListVisible = false;
  endedTasks: SprintTask[] = [];
  multi = [];
  sprintDays;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal,
              private sprintService: SprintService,
              private taskService: TaskService,
              private userService: UserService,
              private boardService: BoardService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.userService.findProjectUserByUserMail(params.projectName).subscribe(userData => {
        const projectUser: ProjectUser = userData;
        if (projectUser.projectRole === ProjectUserRole.PRODUCT_OWNER) {
          this.isProductOwner = true;
        } else {
          // console.log(ProjectUserRole.USER);
          this.isProductOwner = false;
        }
      });


     /* this.sprintService.getActiveSprint(params.boardKey).subscribe(data => {
        this.activeSprint = data;
        console.log('In method');
        console.log(this.activeSprint);

        this.taskService.getDoneTasks(params.sprintKey).subscribe(endedTasksData => {
          this.endedTasks = endedTasksData;

          console.log('****** DONE TASKS *******');
          console.log(this.endedTasks);

          const multix: BurndownChartData[] = [];

          multix[0] = new BurndownChartData();
          multix[0].name = 'Expected result';
          multix[0].series = new Array<NameValue>();


          let nvalue = new NameValue();

          let totalSP = 0;
          let daysNo = this.activeSprint.days;


          this.taskService.getSprintTaskList(this.activeSprint.key).subscribe(currentSprintTasks => {
            currentSprintTasks.forEach(t => {
              totalSP += t.storyPoints;

              console.log('TASK SP');
              console.log(t.storyPoints);
            });

            console.log('Days');
            console.log(daysNo);


            let intermSP = totalSP;
            const step = totalSP / daysNo;
            for (let i = 0; i <= daysNo; i++) {
              nvalue = new NameValue();
              nvalue.name = i;
              if (intermSP >= 0) {
                nvalue.value = intermSP;
              } else {
                nvalue.value = 0;
              }

              intermSP -= step;
              multix[0].series.push(nvalue);
            }

            multix[1] = new BurndownChartData();
            multix[1].name = 'Actual result';
            multix[1].series = new Array<NameValue>();
            intermSP = totalSP;
            let taskIndex = 0;
            const size = endedTasksData.length;
            daysNo = this.getBusinessDatesCount(new Date(this.activeSprint.startDate), new Date());
            console.log(daysNo);
            console.log(this.activeSprint.startDate);
            console.log(new Date(this.activeSprint.startDate));
            console.log(new Date());
            for (let i = 0; i <= daysNo; i++) {
              nvalue = new NameValue();
              let task: TaskDto;
              if (taskIndex < size) {
                task = endedTasksData[taskIndex];
                const workDays = this.getBusinessDatesCount(this.activeSprint.startDate, task.endDate);


                nvalue.name = workDays;
                if (workDays < i) {
                  nvalue.value = intermSP - task.storyPoints;
                  intermSP -= task.storyPoints;
                  taskIndex++;

                  console.log(task.key);
                  console.log(workDays);
                } else {
                  nvalue.name = i;
                  nvalue.value = intermSP;
                }
              } else {
                nvalue.name = i;
                nvalue.value = intermSP;
              }
              multix[1].series.push(nvalue);
            }
            this.multi = multix;
            console.log(this.multi);
          });
          // totalSP = 20;
        });

      });*/
    });

    this.getSprintTasks();
    // this.viewChart();
  }


  private getBusinessDatesCount(startDate, endDate) {
    let count = 0;
    const curDate = new Date(startDate);
    while (curDate <= new Date(endDate)) {
      const dayOfWeek = curDate.getDay();
      if (!((dayOfWeek === 6) || (dayOfWeek === 0))) {
        count++;
      }
      curDate.setDate(curDate.getDate() + 1);
    }
    return count;
  }

  private getActiveSprint() {
    this.route.params.subscribe(params => {
      this.sprintService.getActiveSprint(params.boardKey).subscribe(data => {
        this.activeSprint = data;
        // console.log('Active sprint');
        // console.log(data);

        if (this.sprint.endDate === null) {
          this.endedSprint = false;
          // console.log(this.sprintTasks);
          if (this.activeSprint === null && (this.sprintTasks !== [] || this.sprintTasks !== null)) {
            this.canStartSprint = true;
            this.canEndSprint = false;
            this.canDeleteSprint = true;
          } else {
            if (this.activeSprint.key === this.sprintKey) {
              this.canStartSprint = false;
              this.canEndSprint = true;
              this.canDeleteSprint = false;
            }
          }
        } else {
          this.endedSprint = true;
        }
      });
    });
  }

  private getSprintTasks() {
    this.route.params.subscribe(params => {
      this.sprintKey = params.sprintKey;
      this.sprintService.getCurrentSprint(this.sprintKey).subscribe(data => {
        this.sprint = data;
        this.taskService.getSprintTaskList(this.sprintKey).subscribe(sprintTaskList => {
          this.sprintTasks = sprintTaskList;
        });
        this.getActiveSprint();
      });
    });
  }

  testIfEmpty() {
    if (this.taskTitle.length === 0) {
      this.getSprintTasks();
    }
  }

  searchTask() {
    this.taskService.getSprintTaskByNameContaining(this.taskTitle).subscribe(data => {
      this.sprintTasks = data;
      // console.log(data);
    });
  }

  startSprint() {
    this.sprintService.startSprint(this.sprintKey, this.sprintDays).subscribe(sprint => {
      this.canEndSprint = true;
      this.canStartSprint = false;
      this.canDeleteSprint = false;
    });
  }

  endSprint() {
    this.sprintService.endSprint(this.sprintKey).subscribe(sprint => {
      this.canEndSprint = false;
      this.canStartSprint = false;
      this.canDeleteSprint = false;
      this.endedSprint = true;

      location.reload();
      this.sprintTasks.forEach((task, index) => {
        if (task.status !== 'Done') {
          this.sprintTasks.splice(index, 1);
        }
      });
    });
  }

  addTaskFromBacklog() {
    this.route.params.subscribe(params => {
      const boardKey = params.boardKey;
      this.boardService.getBoardByKey(boardKey).subscribe(boardData => {
        const board: Board = boardData;
        this.taskService.getBacklogTaskList(board.backlog.key).subscribe(taskList => {
          const modalRef = this.modalService.open(AddTaskFromBacklogComponent);
          modalRef.componentInstance.tasks = taskList;
          modalRef.componentInstance.backlog = board.backlog;
          modalRef.componentInstance.sprint = this.sprint;

          modalRef.result.then(list => {
            // console.log(list);
            if ([] !== list || null !== list) {
              list.forEach(task => {
                this.sprintTasks.push(task);
              });
            }
          });

        });
      }, err => {
        // console.log(err.message);
      });
    });


  }

  viewSprintCards() {
    this.isListVisible = false;
    this.showChart = false;
    this.isSprintBoardActive = true;
    this.isCardListVisible = true;
    this.getSprintTasks();
  }

  viewList() {
    this.isListVisible = true;
    this.showChart = false;
    this.isSprintBoardActive = false;
    this.isCardListVisible = false;
    this.getSprintTasks();
  }

  goToBoardItems() {
    this.sprintService.goToBoardItems(this.route);
  }

  deleteSprint() {
    this.sprintService.deleteSprint(this.sprintKey).subscribe(data => {
        this.route.params.subscribe(params => {
          this.router.navigate([`user/projects/${params.projectName}/boards/${params.boardKey}/items`]);
        });
      }, err => {
        console.log(err.mail);
      }
    );
  }

  viewCurrentUserTasks() {
    this.taskService.viewCurrentUserTasks().subscribe(tasks => {
      this.sprintTasks = tasks;
    });
  }

  createReview() {
    this.route.params.subscribe(params => {
      this.router.navigate([`user/projects/${params.projectName}/boards/${params.boardKey}/sprints/${params.sprintKey}/review`]);
    });
  }

  viewChart() {
    this.route.params.subscribe(params => {
      this.showChart = true;
      this.isListVisible = false;
      this.isCardListVisible = false;
      // this.multi = multix;





      this.sprintService.getActiveSprint(params.boardKey).subscribe(data => {
        this.activeSprint = data;
        console.log('In method');
        console.log(this.activeSprint);

        this.taskService.getDoneTasks(params.sprintKey).subscribe(endedTasksData => {
          this.endedTasks = endedTasksData;

          console.log('****** DONE TASKS *******');
          console.log(this.endedTasks);

          const multix: BurndownChartData[] = [];

          multix[0] = new BurndownChartData();
          multix[0].name = 'Expected result';
          multix[0].series = new Array<NameValue>();


          let nvalue = new NameValue();

          let totalSP = 0;
          let daysNo = this.activeSprint.days;


          this.taskService.getSprintTaskList(this.activeSprint.key).subscribe(currentSprintTasks => {
            currentSprintTasks.forEach(t => {
              totalSP += t.storyPoints;

              console.log('TASK SP');
              console.log(t.storyPoints);
            });

            console.log('Days');
            console.log(daysNo);


            let intermSP = totalSP;
            const step = totalSP / daysNo;
            for (let i = 0; i <= daysNo; i++) {
              nvalue = new NameValue();
              nvalue.name = i;
              if (intermSP >= 0) {
                nvalue.value = intermSP;
              } else {
                nvalue.value = 0;
              }

              intermSP -= step;
              multix[0].series.push(nvalue);
            }

            multix[1] = new BurndownChartData();
            multix[1].name = 'Actual result';
            multix[1].series = new Array<NameValue>();
            intermSP = totalSP;
            let taskIndex = 0;
            const size = endedTasksData.length;

            // NOT OK
            daysNo = this.getBusinessDatesCount(new Date(this.activeSprint.startDate), new Date());
            console.log(daysNo);
            console.log(this.activeSprint.startDate);
            console.log(new Date(this.activeSprint.startDate));
            console.log(new Date());
            for (let i = 0; i <= daysNo; i++) {
              nvalue = new NameValue();
              let task: TaskDto;
              if (taskIndex < size) {
                task = endedTasksData[taskIndex];
                const workDays = this.getBusinessDatesCount(this.activeSprint.startDate, task.endDate);


                nvalue.name = workDays;
                if (workDays < i) {
                  nvalue.value = intermSP - task.storyPoints;
                  intermSP -= task.storyPoints;
                  taskIndex++;

                  console.log(task.key);
                  console.log(workDays);
                } else {
                  nvalue.name = i;
                  nvalue.value = intermSP;
                }
              } else {
                nvalue.name = i;
                nvalue.value = intermSP;
              }
              multix[1].series.push(nvalue);
            }
            this.multi = multix;
            console.log(this.multi);
          });
          // totalSP = 20;
        });

      });


    });
  }
}

