import {Component, OnInit} from '@angular/core';
import {OutputUser} from '../dtos/user';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-my-account-component',
  templateUrl: './my.account.html',
  styleUrls: ['./my.account.scss'],
  providers: [UserService]
})


export class MyAccountComponent implements OnInit {
  user: OutputUser;

  constructor(private userService: UserService) {

  }

  ngOnInit(): void {
    // this.userService.getCurrentUser().subscribe(currentUser => {
    //   this.user = currentUser;
    // });
  }
}
