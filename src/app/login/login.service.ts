import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthDto} from '../dtos/auth.dto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../dtos/user';
import {JwtToken} from "../dtos/jwtToken";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class LoginService {
  constructor( private http: HttpClient) {
  }

  login(username, password) {
    const authDto: AuthDto = new AuthDto(username, password, false);
    const myHeaders = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/json'
    });
    console.log(myHeaders);
    return this.http.post<JwtToken>(baseUrl + '/authenticate', authDto, {headers: myHeaders});
  }

  getCurrentUser() {
    const myHeaders = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('jwtToken'),
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/json'
    });
    return this.http.get<User>(baseUrl + '/user/loggedUser', {headers: myHeaders});
  }
}
