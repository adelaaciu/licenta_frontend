import {Component} from '@angular/core';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {UserRole} from '../dtos/role';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.html',
  styleUrls: ['./login.scss'],
  providers: [LoginService, UserService]
})

export class LoginComponent {
  username: string;
  password: string;
  canLogin = true;
  errorMessage = '';

  constructor(private router: Router, private  loginService: LoginService, private userService: UserService) {

  }

  login() {
    this.loginService.login(this.username, this.password).subscribe(response => {
      console.log(response);
      localStorage.setItem('jwtToken', response.idToken);
      this.canLogin = true;
      this.loginService.getCurrentUser().subscribe(userDto => {
        console.log(userDto);
        localStorage.setItem('userMail', userDto.mail);

        // console.log('Admin' + data);
        if (userDto.role === UserRole.ADMIN) {
          this.router.navigate(['/admin/users']);
        }
        // console.log('User' + data)
        if (userDto.role === UserRole.USER) {
          this.router.navigate(['/user']);
        }

      }, err => {
        console.log(err.message);
      });
    }, err => {

      console.log(err.error);
      console.log(JSON.stringify(err.error));

      const strings = JSON.stringify(err.error).split(':');
      const strings1 = strings[1].split('}');
      const string = strings1[0].split('"')[1];

      this.canLogin = false;
      this.errorMessage = string;

    });
  }

}
