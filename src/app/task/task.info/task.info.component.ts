import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TaskService} from '../task.service';
import {BoardUser} from '../../dtos/board.users';
import {CommentDto} from '../../dtos/comments';
import {BacklogTask} from '../../dtos/backlog.task';
import {Backlog} from '../../dtos/backlog';
import {CommentService} from '../../comment/comment.service';
import {UserService} from '../../user/user.service';
import {SprintTask} from '../../dtos/sprint.task';

@Component({
  selector: 'app-task-modal-dialog',
  templateUrl: './task.info.html',
  styleUrls: ['./task.info.scss'],
  providers: [TaskService, CommentService, UserService]
})

export class TaskInfoComponent {
  @Input() isCreationActivated;
  @Input() taskTitle: string;
  @Input() taskKey;
  @Input() description: string;
  @Input() status;
  @Input() watcherMail: string;
  @Input() assigneeMail: string;
  @Input() watcher: BoardUser;
  @Input() assignee: BoardUser;
  @Input() boardKey: string;
  @Input() comments: CommentDto[] = [];
  commentDescription = '';
  @Input() storyPoints: number;
  @Input() priority;
  @Input() task;
  @Input() sprint;
  canCreateTask = true;
  statusList: string[] = [
    'To-Do',
    'Dev-implementation',
    'Dev-testing',
    'Review',
    'QA',
    'Done'
  ];
  priorityList: string[] = [
    'Low',
    'Medium',
    'High'
  ];
  @Input() boardUsers: BoardUser[];
  @Input() backlog: Backlog;

  constructor(public activeModal: NgbActiveModal,
              private taskService: TaskService,
              private userService: UserService) {
  }

  createNewTask() {
    this.getCurrentWatcherAndAssignee();

    const backlogTask: BacklogTask = new BacklogTask(this.backlog, this.taskTitle, this.description, this.status, this.watcher, this.assignee, this.comments, this.storyPoints, this.priority);
    console.log(backlogTask);
    this.taskService.createBacklogTask(backlogTask).subscribe(newTask => {
      this.canCreateTask = true;
      this.activeModal.close(newTask);
    }, err => {
      this.canCreateTask = false;
    });
  }

  updateTaskDetails() {
    console.log(this.boardKey);

    this.userService.getCurrentUser().subscribe(currentUser => {
      if (this.commentDescription.length > 0) {
        const comment = new CommentDto(this.commentDescription, currentUser.mail, this.task.key);
        this.comments.push(comment);
      }
      if (this.backlog != null) {
        this.updateBacklogTask();
      } else {
        this.updateSprintTask();
      }
    });
  }

  private updateBacklogTask() {
    this.getCurrentWatcherAndAssignee();

    const backlogTask: BacklogTask = new BacklogTask(
      this.backlog,
      this.taskTitle,
      this.description,
      this.status,
      this.watcher,
      this.assignee,
      this.comments,
      this.storyPoints,
      this.priority);
    backlogTask.key = this.taskKey;


    console.log(backlogTask);
    this.taskService.updateBacklogTask(backlogTask).subscribe(updatedTask => {
      this.activeModal.close(updatedTask);
    }, err => {
      console.log(err.message);
    });
  }

  private updateSprintTask() {
    this.getCurrentWatcherAndAssignee();
    console.log(this.sprint);
    const sprintTask: SprintTask = new SprintTask(
      this.sprint,
      this.taskTitle,
      this.description,
      this.status,
      this.watcher,
      this.assignee,
      this.comments,
      this.storyPoints,
      this.priority);
    sprintTask.key = this.taskKey;

    this.taskService.updateSprintTask(sprintTask).subscribe(updatedTask => {
      console.log(updatedTask);
      this.activeModal.close(updatedTask);
    }, err => {
      console.log(err.message);
    });
  }

  private getCurrentWatcherAndAssignee() {
    this.boardUsers.forEach(user => {
      if (user.user.user.mail === this.watcherMail) {
        this.watcher = user;
      }
      if (user.user.user.mail === this.assigneeMail) {
        this.assignee = user;
      }
    });
  }
}
