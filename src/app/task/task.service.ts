import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BacklogTask} from '../dtos/backlog.task';
import {SprintTask} from '../dtos/sprint.task';
import {TaskDto} from '../dtos/task';
import {Dashboad} from "../dtos/dashboad";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class TaskService {
  constructor(private http: HttpClient) {

  }

  getBacklogTaskList(backlogKey) {
    return this.http.get<BacklogTask[]>(baseUrl + `/backlogTask/list/${backlogKey}`);
  }

  getSprintTaskList(sprintKey) {
    return this.http.get<SprintTask[]>(baseUrl + `/sprintTask/list/${sprintKey}`);
  }

  createBacklogTask(backlogTask: BacklogTask) {
    return this.http.post<BacklogTask>(baseUrl + `/backlogTask`, backlogTask);
  }

  addTaskToSprint(sprintKey, taskKey) {
    return this.http.put<SprintTask>(baseUrl + `/sprintTask/list/${taskKey}/sprint/${sprintKey}`, null);
  }

  removeTaskFromSprint(sprintKey, taskKey) {
    return this.http.delete(baseUrl + `/backlogTask/list/${sprintKey}/tasks/${taskKey}`);
  }

  removeTaskFromBacklog(taskKey) {
    return this.http.delete(baseUrl + `/backlogTask/list/${taskKey}`);
  }

  getBacklogTaskByNameContaining(taskTitle) {
    return this.http.get<BacklogTask[]>(baseUrl + `/backlogTask/list/containing/${taskTitle}`);
  }

  getSprintTaskByNameContaining(taskTitle) {
    return this.http.get<SprintTask[]>(baseUrl + `/sprintTask/list/containing/${taskTitle}`);
  }


  updateBacklogTask(backlogTask: BacklogTask) {
    return this.http.put<BacklogTask>(baseUrl + `/backlogTask/list`, backlogTask);
  }

  updateSprintTask(sprintTask: SprintTask) {
    return this.http.put<SprintTask>(baseUrl + `/sprintTask/list`, sprintTask);
  }

  changeStatus(taskKey: string, status: string) {
    return this.http.put<TaskDto>(baseUrl + `/task/${taskKey}/status/${status}`, null);
  }

  viewCurrentUserTasks() {
    return this.http.get<SprintTask[]>(baseUrl + `/sprintTask/list/users`);
  }

  getLatestUpdates() {
    return this.http.get<Dashboad[]>(baseUrl + `/task/list/topTen/`);
  }

  getDoneTasks(sprintKey) {
    return this.http.get<SprintTask[]>(baseUrl + `/sprintTask/list/doneTasks/${sprintKey}`);
  }
}
