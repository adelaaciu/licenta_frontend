import {Component, Input} from '@angular/core';
import {TaskDto} from '../../dtos/task';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BacklogService} from '../../backlog/backlog.service';
import {TaskService} from '../task.service';
import {UserService} from '../../user/user.service';
import {CommentService} from '../../comment/comment.service';
import {Backlog} from '../../dtos/backlog';
import {Sprint} from "../../dtos/sprint";

@Component({
  selector: 'app-add-task-from-backlog-component',
  templateUrl: './add.task.from.backlog.html',
  styleUrls: ['./add.task.from.backlog.scss'],
  providers: [BacklogService, TaskService, UserService, CommentService]
})

export class AddTaskFromBacklogComponent {
  @Input() tasks: TaskDto[] = [];
  taskTitle;
  @Input() backlog: Backlog;
  @Input() sprint: Sprint;
  addedTaskList: TaskDto[] = [];

  constructor(private activeModal: NgbActiveModal, private backlogService: BacklogService, private taskService: TaskService, private userService: UserService, private commentService: CommentService) {
  }

  private getBoardTaskList() {
    this.taskService.getBacklogTaskList(this.backlog.key).subscribe(taskList => {
      this.tasks = taskList;
    }, err => {
      console.log(err.message);
    });
  }


  testIfEmpty() {
    if (this.taskTitle.length === 0) {
      this.getBoardTaskList();
    }
  }

  searchTask() {
    this.taskService.getBacklogTaskByNameContaining(this.taskTitle).subscribe(data => {
      this.tasks = data;
      console.log(data);
    });
  }

  addTask(i) {
    this.taskService.addTaskToSprint(this.sprint.key, this.tasks[i].key).subscribe(sprintTask => {
      this.addedTaskList.push(sprintTask);
      this.tasks.splice(i, 1);
    });
  }
}
