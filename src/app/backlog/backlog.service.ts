import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Board} from "../dtos/board";
import {Backlog} from "../dtos/backlog";
import {ActivatedRoute, Router} from "@angular/router";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class BacklogService {

  constructor(private http: HttpClient, private router: Router) {
  }


  getBacklogByBoardKey(backlogKey) {
    return this.http.get<Backlog>(baseUrl + `/backlog/list/${backlogKey}`);
  }

  goToBacklog(board: Board) {
    console.log(board.key);
    this.router.navigate([`user/projects/${board.project.name}/boards/${board.key}/backlog/${board.backlog.key}`]);
  }

  goToBoardItems(activeRoute: ActivatedRoute) {
    activeRoute.params.subscribe(params => {
      this.router.navigate([`user/projects/${params.projectName}/boards/${params.boardKey}/items`]);
    });
  }
}
