import {Component, OnInit} from '@angular/core';
import {BacklogService} from './backlog.service';
import {ActivatedRoute} from '@angular/router';
import {TaskDto} from '../dtos/task';
import {Backlog} from '../dtos/backlog';
import {TaskService} from '../task/task.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TaskInfoComponent} from '../task/task.info/task.info.component';
import {UserService} from '../user/user.service';
import {CommentService} from '../comment/comment.service';
import {ProjectUser} from "../dtos/project.user";
import {ProjectUserRole} from "../dtos/role";

@Component({
  selector: 'app-backlog-component',
  templateUrl: './backlog.html',
  styleUrls: ['./backlog.scss', '../common/common.scss'],
  providers: [BacklogService, TaskService, UserService, CommentService]
})

export class BacklogComponent implements OnInit {
  tasks: TaskDto[] = [];
  backlog: Backlog;
  taskTitle;
  isProductOwner;

  constructor(private modalService: NgbModal, private route: ActivatedRoute, private backlogService: BacklogService, private taskService: TaskService, private userService: UserService, private commentService: CommentService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {

      this.userService.findProjectUserByUserMail(params.projectName).subscribe(userData => {
        const projectUser: ProjectUser = userData;
        if (projectUser.projectRole === ProjectUserRole.PRODUCT_OWNER) {
          this.isProductOwner = true;
        } else {
          console.log(ProjectUserRole.USER);
          this.isProductOwner = false;
        }
      });
    });

    this.getBoardTaskList();
  }

  private getBoardTaskList() {
    this.route.params.subscribe(params => {
      console.log(params.boardKey);
      const backlogKey = params.backlogKey;
      console.log(backlogKey);
      this.backlogService.getBacklogByBoardKey(backlogKey).subscribe(backlogData => {
        this.backlog = backlogData;
        this.taskService.getBacklogTaskList(backlogKey).subscribe(taskList => {
          console.log(taskList);
          this.tasks = taskList;
        });
      }, err => {
        console.log(err.message);
      });
    });
  }

  removeTask(index) {
    this.taskService.removeTaskFromBacklog(this.tasks[index].key).subscribe(data => {
      this.tasks.splice(index, 1);
    });
  }

  displayTaskInfo(index) {
    this.route.params.subscribe(params => {
      const boardKey = params.boardKey;
      console.log('Task info ');
      console.log(boardKey);
      this.userService.getAllBoardUsers(boardKey).subscribe(users => {

        this.commentService.getTaskComments(this.tasks[index].key).subscribe(comm => {
          const modalRef = this.modalService.open(TaskInfoComponent);
          modalRef.componentInstance.boardKey = boardKey;
          modalRef.componentInstance.boardUsers = users;
          modalRef.componentInstance.comments = comm;

          modalRef.componentInstance.isCreationActivated = false;
          modalRef.componentInstance.taskTitle = this.tasks[index].title;
          modalRef.componentInstance.taskKey = this.tasks[index].key;
          modalRef.componentInstance.description = this.tasks[index].description;
          modalRef.componentInstance.status = this.tasks[index].status;
          if (null != this.tasks[index].watcher) {
            modalRef.componentInstance.watcherMail = this.tasks[index].watcher.user.user.mail;
          }
          modalRef.componentInstance.watcher = this.tasks[index].watcher;
          modalRef.componentInstance.assignee = this.tasks[index].assignee;
          if (null != this.tasks[index].assignee) {
            modalRef.componentInstance.assigneeMail = this.tasks[index].assignee.user.user.mail;
          }
          modalRef.componentInstance.comments = this.tasks[index].comments;
          modalRef.componentInstance.storyPoints = this.tasks[index].storyPoints;
          modalRef.componentInstance.priority = this.tasks[index].priority;
          modalRef.componentInstance.task = this.tasks[index];
          modalRef.componentInstance.backlog = this.backlog;

          modalRef.result.then((result) => {
              if (result !== 'Close') {
                this.tasks[index] = result;
              }
            }
          );
        });
      });
    });

  }

  createTask() {
    this.route.params.subscribe(params => {
      console.log(params.boardKey);
      this.userService.getAllBoardUsers(params.boardKey).subscribe(users => {
        const modalRef = this.modalService.open(TaskInfoComponent);
        modalRef.componentInstance.boardUsers = users;
        modalRef.componentInstance.status = 'To-Do';
        modalRef.componentInstance.priority = 'Low';
        modalRef.componentInstance.storyPoints = 0;
        modalRef.componentInstance.isCreationActivated = true;
        modalRef.componentInstance.backlog = this.backlog;

        modalRef.result.then((result) => {
            if (result !== 'Close') {
              this.tasks.push(result);
              location.reload();
            }
          }
        );
      });
    });


  }

  testIfEmpty() {
    if (this.taskTitle.length === 0) {
      this.getBoardTaskList();
    }
  }

  searchTask() {
    this.taskService.getBacklogTaskByNameContaining(this.taskTitle).subscribe(data => {
      this.tasks = data;
      console.log(data);
    });
  }

  goToBoardItems() {
    this.backlogService.goToBoardItems(this.route);
  }

}
